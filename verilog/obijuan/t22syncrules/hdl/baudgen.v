`include "baudgen.vh"

module baudgen #(
  parameter M = `B115200
)(
  input wire clk,
  input wire enable,
  output wire tick
);

localparam N = $clog2(M);

reg [N-1:0] cnt = 0;

always @(posedge clk)
  if (enable)
    cnt <= (cnt == M - 1) ? 0 : cnt + 1;
  else
    cnt <= M - 1;

assign tick = (cnt == 0) ? enable : 0;

endmodule
