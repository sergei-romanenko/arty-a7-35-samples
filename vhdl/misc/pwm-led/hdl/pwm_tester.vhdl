library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;

entity pwm_tester is
port (
  clk: in std_logic;
  btn_incr: in std_logic;
  btn_decr: in std_logic;
  led: out std_logic
);
end pwm_tester;

architecture rtl of pwm_tester is

  component debouncer
  port (
    clk: in std_logic;
    switch_input: in std_logic;
    state: out std_logic;
    trans_up: out std_logic;
    trans_dn: out std_logic
  );
  end component debouncer;

  component pwm
  port (
    clk: in std_logic;
    duty: in unsigned(7 downto 0);
    pwm_pin: out std_logic
  );
  end component pwm;

  constant N: integer := 11;
  constant step: integer := 32;
  signal incr_up, decr_up: std_logic;
  signal duty: unsigned(7 downto 0) := (others => '0');
  -- CLK freq / 128 / 256 = 1.5kHz
  signal prescaler: unsigned(N downto 0) := (others => '0');
begin

  d1: debouncer
    port map (clk => clk, switch_input => btn_incr, trans_up => incr_up);

  d2: debouncer
    port map (clk => clk, switch_input => btn_decr, trans_up => decr_up);

  p: pwm
    port map (clk => prescaler(N), duty => duty, pwm_pin => led);

  process (clk)
  begin
    if rising_edge(clk) then
      prescaler <= prescaler + 1;
      if (incr_up = '1') then
        duty <= shift_left(duty, 1) + 1;
      elsif (decr_up = '1') then
        duty <= shift_right(duty, 1);
      end if;
    end if;
  end process;

end rtl;
