library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;

entity pwm is
port (
  clk: in std_logic;
  duty: in unsigned(7 downto 0);
  pwm_pin: out std_logic
);
end pwm;

architecture rtl of pwm is
  signal count: unsigned(7 downto 0) := (others => '0');
begin
  process (clk)
  begin
    if rising_edge(clk) then
      count <= count + 1;
      pwm_pin <= '1' when (count < duty) else '0';
    end if;
  end process;
end rtl;
