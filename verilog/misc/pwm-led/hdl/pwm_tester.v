`include "debouncer.v"
`include "pwm.v"

`default_nettype none

module pwm_tester
#(
  parameter nleds = 4
)(
  input wire clk,
  input wire btn_incr,
  input wire btn_decr,
  output wire [nleds-1:0] leds
);

localparam N = 11;
localparam step = 32;

assign leds[nleds-1:1] = 0;

wire incr_up, decr_up;
debouncer d1(.clk (clk), .switch_input (btn_incr), .trans_up (incr_up));
debouncer d2(.clk (clk), .switch_input (btn_decr), .trans_up (decr_up));

reg [7:0] duty = 0;
reg [N:0] prescaler = 0; // CLK freq / 128 / 256 = 1.5kHz

pwm p(.pwm_clk (prescaler[N]), .duty (duty), .pwm_pin (leds[0]));

always @(posedge clk)
begin
  prescaler <= prescaler + 1;
  if (incr_up && duty <= 255 - step)
    duty <= duty + 32;
  if (decr_up && duty >= step)
    duty <= duty - 32;
end
endmodule
