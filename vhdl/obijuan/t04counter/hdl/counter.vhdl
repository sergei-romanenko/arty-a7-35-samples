library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
generic (
  width: integer := 30;
  nleds: integer := 4
);
port (
  clk: in std_logic;
  leds: out std_logic_vector(nleds - 1 downto 0)
);
end counter;

architecture rtl of counter is
  signal cnt: unsigned(width - 1 downto 0) := (others => '0');
begin
  leds <= std_logic_vector(cnt(width - 1 downto width - nleds));

  process (clk) is
  begin
    if rising_edge(clk) then
      cnt <= cnt + 1;
    end if;
  end process;
end rtl;
