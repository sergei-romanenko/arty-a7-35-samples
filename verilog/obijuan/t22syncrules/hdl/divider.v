`include "divider.vh"

module divider(input wire clk_in, output wire clk_out);

parameter M = 104;
localparam N = $clog2(M);

reg [N-1:0] cnt = 0;
assign clk_out = cnt[N-1];

always @(posedge clk_in)
  cnt <= (cnt == M - 1) ? 0 : cnt + 1;

endmodule
