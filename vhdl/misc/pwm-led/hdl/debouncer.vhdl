library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;

entity debouncer is
port(
  clk: in std_logic;
  switch_input: in std_logic;
  state: out std_logic;
  trans_up: out std_logic;
  trans_dn: out std_logic
);
end debouncer;

architecture rtl of debouncer is
  -- Synchronize the switch input to the clock
  signal sync_0, sync_1: std_logic;
  -- Debounce the switch
  signal count: unsigned(16 downto 0) := (others => '0');
  -- true when all bits of count are 1's
  signal finished: std_logic;
  signal idle: std_logic;
begin
  finished <= and count;
  idle <= '1' when (state = sync_1) else '0';

  trans_dn <= not idle and finished and not state;
  trans_up <= not idle and finished and state;

  process(clk)
  begin
    if rising_edge(clk) then
      sync_0 <= switch_input;
      sync_1 <= sync_0;
    end if;
  end process;

  process(clk)
  begin
    if rising_edge(clk) then
      if (idle) then
        count <= (others => '0');
      else
        count <= count + 1;
        if (finished) then
          state <= not state;
        end if;
      end if;
    end if;
  end process;

end rtl;
