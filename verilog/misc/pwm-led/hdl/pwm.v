`ifndef _pwm_
`define _pwm_

`default_nettype none

module pwm(
  input wire pwm_clk,
  input wire [7:0] duty,
  output reg pwm_pin
);

reg [7:0] count = 0;
always @(posedge pwm_clk)
begin
  count <= count + 1;
  pwm_pin <= (count < duty);
end
endmodule

`endif
