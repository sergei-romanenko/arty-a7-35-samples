`ifndef _counter_
`define _counter_

module counter #(
  parameter WIDTH = 30,
  parameter NLEDS = 8
)(
  input clk,
  output wire [NLEDS-1: 0] leds
);

reg [WIDTH-1: 0] cnt = 0;
assign leds[NLEDS-1: 0] = cnt[WIDTH-1: WIDTH-NLEDS];

always @(posedge clk) begin
	cnt <= cnt + 1;
end

endmodule

`endif
