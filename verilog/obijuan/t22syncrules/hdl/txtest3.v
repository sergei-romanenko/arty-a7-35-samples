`default_nettype none

`include "baudgen.vh"
//`include "baudgen.v"

// The module that continuously sends a character.
// The output tx is in a register.
module txtest3 #(
  parameter BAUD = 10416, // (round(100MHz / 9600)) - 1 // `B115200,
  parameter DELAY = 25000000 //`T_250ms
)(
  input wire clk,
  output reg tx = 1
);

wire load;
reg enable = 0;

always @(posedge clk)
  enable <= load;

reg [9:0] shifter;
wire tick;

always @(posedge clk)
  if (enable == 0)
    shifter <= {"K", 2'b01};
  else if (enable == 1 && tick == 1)
    shifter <= {1'b1, shifter[9:1]};

always @(posedge clk)
  tx <= enable ? shifter[0] : 1;

baudgen #(BAUD)
  BAUD0 (.clk(clk), .enable(enable), .tick(tick));

divider #(DELAY)
  DIV0 (.clk_in(clk), .clk_out(load));

endmodule
