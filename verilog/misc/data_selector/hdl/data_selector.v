`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/18/2018 11:17:18 PM
// Design Name: 
// Module Name: data_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`default_nettype none

module data_selector(
  input wire A,
  input wire B,
  input wire SEL,
  output reg Q
  );
  
    
always @(A or B or SEL)
begin
  if (SEL)
    Q = A;
  else
    Q = B;
end

endmodule
