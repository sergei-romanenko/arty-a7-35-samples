`ifndef _debouncer_
`define _debouncer_

`default_nettype none

module debouncer(
  input wire clk,
  input wire switch_input,
  output reg state,
  output wire trans_up,
  output wire trans_dn
);

// Synchronize the switch input to the clock
reg sync_0, sync_1;

// Debounce the switch
reg [16:0] count = 0;
// true when all bits of count are 1's
wire finished = &count;

wire idle = (state == sync_1);

assign trans_dn = ~idle & finished & ~state;
assign trans_up = ~idle & finished & state;

always @(posedge clk)
begin
  sync_0 <= switch_input;
  sync_1 <= sync_0;
end

always @(posedge clk)
begin
  if (idle)
    count <= 0;
  else
  begin
    count <= count + 1;
    if (finished)
      state <= ~state;
  end
end

endmodule

`endif
