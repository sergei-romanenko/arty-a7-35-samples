library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

entity counter_tb is
end counter_tb;

architecture behavior of counter_tb is
  constant width: integer := 6;
  constant nleds: integer := 4;
  constant half_period: time := 5 ns;

  signal clk: std_logic := '0';
  signal finished: std_logic := '0';

  signal leds: std_logic_vector(nleds - 1 downto 0);

  signal cnt: unsigned(width - 1 downto 0) := (others => '0');
  signal high: std_logic_vector(nleds - 1 downto 0);

  component counter
  generic (
    width: integer := 33;
    nleds: integer := 4
  );
  port (
    clk: in std_logic;
    leds: out std_logic_vector(nleds - 1 downto 0)
  );
  end component counter;

begin

  counter_inst: counter
    generic map (width => width, nleds => nleds)
    port map (clk => clk, leds => leds);

  clk <= not clk after half_period when finished /= '1' else '0';

  high <= std_logic_vector(cnt(width - 1 downto width - nleds));

  process (clk) is
  begin
    if rising_edge(clk) then
      cnt <= cnt + 1;
    end if;
  end process;

  process (clk) is
  begin
    if falling_edge(clk) then
      assert high = leds
        report "high /= leds" severity error;
    end if;
  end process;

  process
  begin
    wait for 5 ns;
    assert cnt = 0
      report "cnt /= 0" severity error;

    wait for 20 * 10 ns;
    finished <= '1';
    report "Finish!";
    wait for 20 ns;
    -- assert false report "Stop!" severity failure;
    -- wait;
    finish(1);
  end process;
end behavior;
