library ieee;
use ieee.std_logic_1164.all;
-- use ieee.std_logic_arith.all;
-- use ieee.std_logic_unsigned.all;

entity data_selector is port(
  A, B : in std_logic;
  SEL  : in std_logic;
  Q    : out std_logic);
end data_selector;

architecture rtl of data_selector is
begin
  process (A, B, SEL) is
  begin
    if (SEL = '1') then
      Q <= A;
    else
      Q <= B;
    end if;
  end process;
end rtl;
